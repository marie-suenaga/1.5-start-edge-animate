/***********************
* Adobe Edge Animate コンポジションアクション
*
* このファイルを編集する際には注意が必要です。必ず関数シグニチャと
* 「Edge」で始まるコメントを保持して、 Adobe Edge 内からこれらのアクションを
* 操作可能な状態にしておいてください。 
*
***********************/
(function($, Edge, compId){
var Composition = Edge.Composition, Symbol = Edge.Symbol; // よく使用する Edge クラスのエイリアス

   //Edge symbol: 'stage'
   (function(symbolName) {
      
      
      Symbol.bindElementAction(compId, symbolName, "${_button01}", "click", function(sym, e) {
         // マウスクリックのコードをここに挿入します
         // 指定した位置からタイムラインを再生 (ミリ秒またはラベル)
         sym.play("slide01");

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${_button02}", "click", function(sym, e) {
         // マウスクリックのコードをここに挿入します
         // 指定した位置からタイムラインを再生 (ミリ秒またはラベル)
         sym.play("slide02");

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${_button03}", "click", function(sym, e) {
         // マウスクリックのコードをここに挿入します
         // 指定した位置からタイムラインを再生 (ミリ秒またはラベル)
         sym.play("slide03");

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${_slide03_animal}", "mouseover", function(sym, e) {
         // 指定した位置からタイムラインを再生 (ミリ秒またはラベル)
         sym.play("animal");
         // マウスがオブジェクトに重なったときに実行されるコードを挿入します

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${_button_on}", "click", function(sym, e) {
         // マウスクリックのコードをここに挿入します
         // 指定した位置からタイムラインを再生 (ミリ秒またはラベル)
         sym.play("slide03");

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 7250, function(sym, e) {
         // 指定した位置でタイムラインを停止 (ミリ秒またはラベル)
         sym.stop("animate");

      });
      //Edge binding end

   })("stage");
   //Edge symbol end:'stage'

})(jQuery, AdobeEdge, "slide");