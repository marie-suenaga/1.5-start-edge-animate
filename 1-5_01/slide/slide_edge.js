/**
 * Adobe Edge: symbol definitions
 */
(function($, Edge, compId){
//images folder
var im='images/';

var fonts = {};


var resources = [
];
var symbols = {
"stage": {
   version: "1.5.0",
   minimumCompatibleVersion: "1.5.0",
   build: "1.5.0.217",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
         dom: [
         {
            id:'slide01',
            type:'image',
            rect:['0','0','700px','296px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"slide01.jpg",'0px','0px']
         },
         {
            id:'slide02',
            type:'image',
            rect:['0','0','700px','296px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"slide02.jpg",'0px','0px']
         },
         {
            id:'slide03',
            type:'image',
            rect:['0','0','700px','296px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"slide03.jpg",'0px','0px']
         },
         {
            id:'slide03_animal',
            type:'image',
            rect:['3px','0','107px','142px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"slide03_animal.jpg",'0px','0px']
         },
         {
            id:'button01',
            type:'rect',
            rect:['20px','308px','32px','23px','auto','auto'],
            title:'ボタン',
            cursor:['pointer'],
            borderRadius:["10px","10px","10px","10px"],
            fill:["rgba(235,119,119,1.00)"],
            stroke:[1,"rgba(255,138,138,1.00)","none"],
            boxShadow:["",0,0,0,0,"rgba(255,212,212,0.65)"],
            filter:[0,0,1,1,0,0,0,0,"rgba(0,0,0,0)",0,0,0]
         },
         {
            id:'button02',
            type:'rect',
            rect:['68px','308px','32px','23px','auto','auto'],
            title:'ボタン',
            cursor:['pointer'],
            borderRadius:["10px","10px","10px","10px"],
            fill:["rgba(235,119,119,1.00)"],
            stroke:[1,"rgba(255,138,138,1.00)","none"],
            boxShadow:["",0,0,0,0,"rgba(255,212,212,0.65)"],
            filter:[0,0,1,1,0,0,0,0,"rgba(0,0,0,0)",0,0,0]
         },
         {
            id:'button03',
            type:'rect',
            rect:['115px','308px','32px','23px','auto','auto'],
            title:'ボタン',
            cursor:['pointer'],
            borderRadius:["10px","10px","10px","10px"],
            fill:["rgba(235,119,119,1.00)"],
            stroke:[1,"rgba(255,138,138,1.00)","none"],
            boxShadow:["",0,0,0,0,"rgba(255,212,212,0.65)"],
            filter:[0,0,1,1,0,0,0,0,"rgba(0,0,0,0)",0,0,0]
         },
         {
            id:'button_on',
            type:'rect',
            rect:['20px','308px','32px','23px','auto','auto'],
            title:'ボタン',
            cursor:['pointer'],
            borderRadius:["10px","10px","10px","10px"],
            opacity:0.66766357421875,
            fill:["rgba(255,255,255,1.00)"],
            stroke:[1,"rgba(255,138,138,1.00)","none"],
            boxShadow:["",0,0,0,0,"rgba(255,212,212,0.65)"],
            filter:[0,0,1,1,0,0,0,0,"rgba(0,0,0,0)",0,0,0]
         }],
         symbolInstances: [

         ]
      },
   states: {
      "Base State": {
         "${_slide03}": [
            ["style", "top", '-322px'],
            ["style", "left", '-9px']
         ],
         "${_slide03_animal}": [
            ["style", "top", '-302px'],
            ["style", "left", '3px'],
            ["transform", "rotateZ", '15deg']
         ],
         "${_slide02}": [
            ["style", "top", '-322px'],
            ["style", "left", '-9px']
         ],
         "${_button01}": [
            ["color", "background-color", 'rgba(235,119,119,1.00)'],
            ["style", "left", '20px'],
            ["subproperty", "boxShadow.blur", '0px'],
            ["color", "border-color", 'rgba(255,138,138,1.00)'],
            ["subproperty", "filter.blur", '0px'],
            ["style", "border-style", 'none'],
            ["style", "border-width", '1px'],
            ["style", "width", '32px'],
            ["style", "top", '308px'],
            ["subproperty", "boxShadow.color", 'rgba(255,212,212,0.65)'],
            ["subproperty", "filter.drop-shadow.offsetV", '0px'],
            ["subproperty", "filter.drop-shadow.offsetH", '0px'],
            ["style", "height", '23px'],
            ["subproperty", "boxShadow.offsetV", '0px'],
            ["subproperty", "boxShadow.offsetH", '0px'],
            ["style", "cursor", 'pointer']
         ],
         "${_slide01}": [
            ["style", "top", '-322px'],
            ["style", "left", '-9px']
         ],
         "${_button02}": [
            ["color", "background-color", 'rgba(235,119,119,1.00)'],
            ["style", "cursor", 'pointer'],
            ["subproperty", "boxShadow.blur", '0px'],
            ["subproperty", "filter.blur", '0px'],
            ["subproperty", "boxShadow.offsetV", '0px'],
            ["style", "border-style", 'none'],
            ["style", "left", '68px'],
            ["style", "width", '32px'],
            ["style", "top", '308px'],
            ["subproperty", "boxShadow.color", 'rgba(255,212,212,0.65)'],
            ["subproperty", "filter.drop-shadow.offsetH", '0px'],
            ["subproperty", "filter.drop-shadow.offsetV", '0px'],
            ["style", "height", '23px'],
            ["color", "border-color", 'rgba(255,138,138,1.00)'],
            ["subproperty", "boxShadow.offsetH", '0px'],
            ["style", "border-width", '1px']
         ],
         "${_Stage}": [
            ["color", "background-color", 'rgba(255,255,255,1)'],
            ["style", "width", '690px'],
            ["style", "height", '340px'],
            ["style", "overflow", 'hidden']
         ],
         "${_button_on}": [
            ["style", "opacity", '0.66766357421875'],
            ["style", "left", '20px'],
            ["color", "border-color", 'rgba(255,138,138,1.00)'],
            ["subproperty", "boxShadow.offsetH", '0px'],
            ["color", "background-color", 'rgba(255,255,255,1.00)'],
            ["subproperty", "boxShadow.color", 'rgba(255,212,212,0.65)'],
            ["style", "border-style", 'none'],
            ["style", "border-width", '1px'],
            ["style", "width", '32px'],
            ["style", "top", '308px'],
            ["subproperty", "boxShadow.offsetV", '0px'],
            ["subproperty", "filter.drop-shadow.offsetV", '0px'],
            ["subproperty", "boxShadow.blur", '0px'],
            ["style", "height", '23px'],
            ["subproperty", "filter.blur", '0px'],
            ["style", "cursor", 'pointer'],
            ["subproperty", "filter.drop-shadow.offsetH", '0px']
         ],
         "${_button03}": [
            ["color", "background-color", 'rgba(235,119,119,1.00)'],
            ["style", "border-width", '1px'],
            ["subproperty", "boxShadow.blur", '0px'],
            ["subproperty", "boxShadow.offsetV", '0px'],
            ["color", "border-color", 'rgba(255,138,138,1.00)'],
            ["style", "border-style", 'none'],
            ["style", "cursor", 'pointer'],
            ["style", "width", '32px'],
            ["style", "top", '308px'],
            ["subproperty", "boxShadow.color", 'rgba(255,212,212,0.65)'],
            ["subproperty", "filter.drop-shadow.offsetV", '0px'],
            ["subproperty", "filter.drop-shadow.offsetH", '0px'],
            ["style", "height", '23px'],
            ["subproperty", "filter.blur", '0px'],
            ["subproperty", "boxShadow.offsetH", '0px'],
            ["style", "left", '115px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 8250,
         autoPlay: true,
         labels: {
            "slide01": 1250,
            "slide02": 3500,
            "slide03": 5500,
            "animal": 7250
         },
         timeline: [
            { id: "eid22", tween: [ "style", "${_slide03_animal}", "top", '154px', { fromValue: '-302px'}], position: 5151, duration: 1639, easing: "easeInOutBounce" },
            { id: "eid54", tween: [ "transform", "${_slide03_animal}", "rotateZ", '-7deg', { fromValue: '15deg'}], position: 7250, duration: 500, easing: "swing" },
            { id: "eid55", tween: [ "transform", "${_slide03_animal}", "rotateZ", '10deg', { fromValue: '-7deg'}], position: 7750, duration: 500, easing: "swing" },
            { id: "eid23", tween: [ "style", "${_slide03}", "top", '-302px', { fromValue: '-322px'}], position: 0, duration: 4000, easing: "easeInOutElastic" },
            { id: "eid20", tween: [ "style", "${_slide03}", "top", '0px', { fromValue: '-302px'}], position: 4000, duration: 2000, easing: "easeInOutElastic" },
            { id: "eid24", tween: [ "style", "${_slide02}", "top", '-302px', { fromValue: '-322px'}], position: 0, duration: 2000, easing: "easeInOutElastic" },
            { id: "eid21", tween: [ "style", "${_slide02}", "top", '0px', { fromValue: '-302px'}], position: 2000, duration: 2000, easing: "easeInOutElastic" },
            { id: "eid19", tween: [ "style", "${_slide01}", "top", '0px', { fromValue: '-322px'}], position: 0, duration: 2000, easing: "easeInOutElastic" },
            { id: "eid71", tween: [ "style", "${_button_on}", "left", '20px', { fromValue: '20px'}], position: 0, duration: 0, easing: "swing" },
            { id: "eid69", tween: [ "style", "${_button_on}", "left", '68px', { fromValue: '20px'}], position: 3000, duration: 0, easing: "swing" },
            { id: "eid70", tween: [ "style", "${_button_on}", "left", '115px', { fromValue: '68px'}], position: 5151, duration: 0, easing: "swing" }         ]
      }
   }
}
};


Edge.registerCompositionDefn(compId, symbols, fonts, resources);

/**
 * Adobe Edge DOM Ready Event Handler
 */
$(window).ready(function() {
     Edge.launchComposition(compId);
});
})(jQuery, AdobeEdge, "slide");
