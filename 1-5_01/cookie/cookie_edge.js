/**
 * Adobe Edge: symbol definitions
 */
(function($, Edge, compId){
//images folder
var im='images/';

var fonts = {};


var resources = [
];
var symbols = {
"stage": {
   version: "1.5.0",
   minimumCompatibleVersion: "1.5.0",
   build: "1.5.0.217",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
         dom: [
         {
            id:'cookie_01',
            type:'image',
            rect:['0','0','750px','493px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"cookie_00.jpg",'0px','0px']
         },
         {
            id:'cookie_00',
            type:'image',
            rect:['0%','0%','750px','493px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"cookie_01.jpg",'0%','0%','100%','auto']
         }],
         symbolInstances: [

         ]
      },
   states: {
      "Base State": {
         "${_Stage}": [
            ["color", "background-color", 'rgba(255,255,255,1)'],
            ["style", "overflow", 'hidden'],
            ["style", "height", '100%'],
            ["style", "width", '100%']
         ],
         "${_cookie_00}": [
            ["style", "display", 'block'],
            ["style", "background-size", [100,'auto'], {valueTemplate:'@@0@@% @@1@@'} ]
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 1000,
         autoPlay: true,
         timeline: [
            { id: "eid40", tween: [ "style", "${_cookie_00}", "display", 'block', { fromValue: 'block'}], position: 0, duration: 0 },
            { id: "eid41", tween: [ "style", "${_cookie_00}", "display", 'none', { fromValue: 'block'}], position: 500, duration: 0 },
            { id: "eid42", tween: [ "style", "${_cookie_00}", "display", 'block', { fromValue: 'none'}], position: 1000, duration: 0 }         ]
      }
   }
}
};


Edge.registerCompositionDefn(compId, symbols, fonts, resources);

/**
 * Adobe Edge DOM Ready Event Handler
 */
$(window).ready(function() {
     Edge.launchComposition(compId);
});
})(jQuery, AdobeEdge, "cookie");
